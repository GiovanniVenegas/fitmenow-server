<?php
App::uses('AppController', 'Controller');
/**
 * Instructors Controller
 *
 * @property Instructor $Instructor
 * @property PaginatorComponent $Paginator
 */
class InstructorsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Instructor->recursive = 0;
		$this->set('instructors', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Instructor->exists($id)) {
			throw new NotFoundException(__('Invalid instructor'));
		}
		$options = array('conditions' => array('Instructor.' . $this->Instructor->primaryKey => $id));
		$this->set('instructor', $this->Instructor->find('first', $options));
	}

/**
 * add methoda
 *
 * @return void
 */
	public function add($user_id = NULL, $user_group_id = NULL) {
		$this->set('user_id',$user_id);
		$this->set('user_group_id',$user_group_id);
		debug($this->request->data);
		if ($this->request->is('post')) {
			
		 $folderName = 'instructors';
		 $slug = md5(gmdate("Y-m-d")); // substr(slug($this->request->data['Instructor']['name']),0,40);
		 
		 $image_path = $this->Upload->upload_image_and_thumbnail( 
		 					$this->request->data,"name1",600,296,$folderName,false,$slug);
		 $this->request->data['Instructor']['image']= $image_path;

			$this->Instructor->create();
			if ($this->Instructor->save($this->request->data)) {
				$this->Session->setFlash(__('The instructor has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The instructor could not be saved. Please, try again.'));
			}
		}
		$users = $this->Instructor->User->find('list');
		$gyms = $this->Instructor->Gym->find('list');
		$certifications = $this->Instructor->Certification->find('list');
		$this->set(compact('users', 'gyms', 'certifications'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Instructor->exists($id)) {
			throw new NotFoundException(__('Invalid instructor'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
	//	 $folderName = 'instructors';
	//	 $slug = md5(gmdate("Y-m-d")); // substr(slug($this->request->data['Instructor']['name']),0,40);
	//	 $image_path = $this->Upload->upload_image_and_thumbnail( 
	//	 					$this->request->data,"name1",600,296,$folderName,false,$slug);
	//	 $this->request->data['Instructor']['image']= $image_path;


			if ($this->Instructor->save($this->request->data)) {
				$this->Session->setFlash(__('The instructor has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The instructor could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Instructor.' . $this->Instructor->primaryKey => $id));
			$this->request->data = $this->Instructor->find('first', $options);
		}
		$users = $this->Instructor->User->find('list');
		$gyms = $this->Instructor->Gym->find('list');
		$certifications = $this->Instructor->Certification->find('list');
		$this->set(compact('users', 'gyms', 'certifications'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Instructor->id = $id;
		if (!$this->Instructor->exists()) {
			throw new NotFoundException(__('Invalid instructor'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Instructor->delete()) {
			$this->Session->setFlash(__('The instructor has been deleted.'));
		} else {
			$this->Session->setFlash(__('The instructor could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
